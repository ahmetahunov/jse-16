package ru.ahmetahunov.tm.integration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.ahmetahunov.tm.api.endpoint.AdminEndpoint;
import ru.ahmetahunov.tm.api.endpoint.SessionEndpoint;
import ru.ahmetahunov.tm.api.endpoint.UserDTO;
import ru.ahmetahunov.tm.api.endpoint.UserEndpoint;
import ru.ahmetahunov.tm.endpoint.AdminEndpointImplService;
import ru.ahmetahunov.tm.endpoint.SessionEndpointImplService;
import ru.ahmetahunov.tm.endpoint.UserEndpointImplService;

public class UserEndpointIT {

	@NotNull
	private static final SessionEndpoint sessionEndpoint = new SessionEndpointImplService().getSessionEndpointImplPort();

	@NotNull
	private static final UserEndpoint userEndpoint = new UserEndpointImplService().getUserEndpointImplPort();

	@NotNull
	private static final AdminEndpoint adminEndpoint = new AdminEndpointImplService().getAdminEndpointImplPort();

	@Nullable
	private static String tokenAdmin;

	@BeforeClass
	public static void init() throws Exception {
		tokenAdmin = sessionEndpoint.createSession("admin", "admin");
	}

	@AfterClass
	public static void clean() throws Exception {
		sessionEndpoint.removeSession(tokenAdmin);
	}

	@After
	public void removeUser() throws Exception {
		@Nullable final UserDTO user = adminEndpoint.userFindByLogin(tokenAdmin,"testUser");
		if (user != null) adminEndpoint.userRemove(tokenAdmin, user.getId());
	}

	@Test
	public void createUserExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.createUser(null, "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.createUser("testUser", null)
		);
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.createUser("", "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.createUser("testUser", "")
		);
	}

	@Test
	public void createUserOkTest() throws Exception {
		@Nullable final UserDTO user = userEndpoint.createUser("testUser", "user");
		Assert.assertNotNull(user);
		Assert.assertEquals("testUser", user.getLogin());
	}

	@Test
	public void updatePasswordExceptionTest() throws Exception {
		@Nullable final UserDTO user = userEndpoint.createUser("testUser", "user");
		@NotNull final String token = sessionEndpoint.createSession(user.getLogin(), "user");
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.updatePassword(null, "user", "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.updatePassword("", "user", "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.updatePassword(token, null, "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.updatePassword(token, "", "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.updatePassword(token, "user", null)
		);
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.updatePassword(token, "user", "")
		);
	}

	@Test
	public void updatePasswordOkTest() throws Exception {
		@Nullable final UserDTO user = userEndpoint.createUser("testUser", "user");
		@NotNull final String token = sessionEndpoint.createSession(user.getLogin(), "user");
		userEndpoint.updatePassword(token, "user", "0000");
		Assert.assertThrows(
				Exception.class,
				() -> sessionEndpoint.createSession(user.getLogin(), "user")
		);
		@NotNull final String token2 = sessionEndpoint.createSession(user.getLogin(), "0000");
		Assert.assertNotNull(token2);
	}

	@Test
	public void updateLoginExceptionTest() throws Exception {
		@Nullable final UserDTO user = userEndpoint.createUser("testUser", "user");
		@NotNull final String token = sessionEndpoint.createSession(user.getLogin(), "user");
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.updateLogin(null, "updated")
		);
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.updateLogin("", "updated")
		);
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.updateLogin(token, "")
		);
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.updateLogin(token, null)
		);
	}

	@Test
	public void updateLoginOkTest() throws Exception {
		@Nullable final UserDTO user = userEndpoint.createUser("updated", "user");
		@NotNull final String token = sessionEndpoint.createSession(user.getLogin(), "user");
		userEndpoint.updateLogin(token, "testUser");
		Assert.assertThrows(
				Exception.class,
				() -> sessionEndpoint.createSession(user.getLogin(), "user")
		);
		@NotNull final String token2 = sessionEndpoint.createSession("testUser", "user");
		Assert.assertNotNull(token2);
	}

	@Test
	public void findUserExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.findUser(null)
		);
		Assert.assertThrows(
				Exception.class,
				() -> userEndpoint.findUser("")
		);
	}

	@Test
	public void findUserOkTest() throws Exception {
		@Nullable final UserDTO user = userEndpoint.createUser("testUser", "user");
		@NotNull final String token = sessionEndpoint.createSession(user.getLogin(), "user");
		@Nullable final UserDTO found = userEndpoint.findUser(token);
		Assert.assertNotNull(found);
		Assert.assertEquals(user.getId(), found.getId());
		Assert.assertEquals(user.getLogin(), found.getLogin());
	}

}
