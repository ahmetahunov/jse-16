package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import java.lang.Exception;
import java.util.List;

@NoArgsConstructor
public final class ProjectByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-by-name";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects with entered part of name.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = serviceLocator.getStateService().getSession();
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[PROJECTS BY NAME]");
        @NotNull final String projectName = terminalService.getAnswer("Please enter project name: ");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectByName(session, projectName);
        int i = 1;
        for (@NotNull final ProjectDTO project : projects) {
            terminalService.writeMessage(String.format("%d.%s ID:%s", i++, project.getName(), project.getId()));
        }
    }

}
