package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.UserDTO;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserListAdminCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all users.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String session = serviceLocator.getStateService().getSession();
        terminalService.writeMessage("[USER LIST]");
        int i = 1;
        for (@NotNull final UserDTO user : serviceLocator.getAdminEndpoint().findAllUsers(session)) {
            terminalService.writeMessage(String.format("%d. %s ID:%s", i++, user.getLogin(), user.getId()));
        }
    }

}
