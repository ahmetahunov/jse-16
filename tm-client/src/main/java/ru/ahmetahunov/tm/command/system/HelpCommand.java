package ru.ahmetahunov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IStateService;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all available commands.";
    }

    @Override
    public void execute() {
        @NotNull final IStateService stateService = serviceLocator.getStateService();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        for (@NotNull AbstractCommand command : stateService.getCommands()) {
            terminalService.writeMessage(String.format("%-22s: %s", command.getName(), command.getDescription()));
        }
    }

}