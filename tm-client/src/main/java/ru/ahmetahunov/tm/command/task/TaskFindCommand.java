package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import java.lang.Exception;
import java.util.List;

@NoArgsConstructor
public class TaskFindCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-find";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Search task by part of name or description.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = serviceLocator.getStateService().getSession();
        @NotNull final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[FIND TASK]");
        @NotNull final String searchPhrase = terminalService.getAnswer("Please enter task name or description: ");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTaskByNameOrDesc(session, searchPhrase);
        int i = 1;
        for (@NotNull final TaskDTO task : tasks) {
            terminalService.writeMessage(
                    String.format(
                            "%d.%s ID:%s\nDescription:%s",
                            i++,
                            task.getName(),
                            task.getId(),
                            task.getDescription())
            );
        }
        if (tasks.isEmpty()) terminalService.writeMessage("Not found.");
    }

}
