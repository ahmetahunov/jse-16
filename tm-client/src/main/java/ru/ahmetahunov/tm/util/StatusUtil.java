package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.endpoint.Status;
import ru.ahmetahunov.tm.exception.FailedOperationException;

public final class StatusUtil {

    @NotNull
    public static Status getStatus(@NotNull final String status) throws FailedOperationException {
        if ("planned".equals(status.toLowerCase())) return Status.PLANNED;
        if ("in-progress".equals(status.toLowerCase())) return Status.IN_PROGRESS;
        if ("done".equals(status.toLowerCase())) return Status.DONE;
        throw new FailedOperationException("Unknown status");
    }

}
