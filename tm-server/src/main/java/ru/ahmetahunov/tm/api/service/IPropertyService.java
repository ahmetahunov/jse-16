package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

	@Nullable
	public String getSecretPhrase();

}
