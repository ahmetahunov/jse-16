package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import java.util.List;

public interface IProjectRepository {

    public void persist(@NotNull Project project);

    public void merge(@NotNull Project project);

    @Nullable
    public Project findOne(@NotNull String id);

    @Nullable
    public Project findOneByUserId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    public List<Project> findAll();

    @NotNull
    public List<Project> findAllByUserId(@NotNull String userId);

    @NotNull
    public List<Project> findAllWithComparator(@NotNull String userId, @NotNull String comparator);

    @NotNull
    public List<Project> findByName(@NotNull String userId, @NotNull String projectName);

    @NotNull
    public List<Project> findByDescription(@NotNull String userId, @NotNull String description);

    @NotNull
    public List<Project> findByNameOrDesc(@NotNull String userId, @NotNull String searchPhrase);

    public void removeAll(@NotNull String userId);

    public void removeById(@NotNull String id) throws InterruptedOperationException;

    public void remove(@NotNull String userId, @NotNull String id) throws InterruptedOperationException;

}
