package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.dto.SessionDTO;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import java.util.List;

public interface ISessionService extends IAbstractService<Session> {

	@Nullable
	public Session findOne(String id, String userId);

	@NotNull
	public List<Session> findAll(@NotNull String userId);

	public void remove(String id, String userId) throws InterruptedOperationException;

	public void validate(SessionDTO session) throws AccessForbiddenException, InterruptedOperationException;

	public void validate(SessionDTO session, Role role) throws AccessForbiddenException, InterruptedOperationException;

}
