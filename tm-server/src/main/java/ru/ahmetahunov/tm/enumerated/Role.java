package ru.ahmetahunov.tm.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
public enum Role {

    USER("User"),
    ADMINISTRATOR("Administrator");

    @Getter
    @NotNull
    private final String displayName;

}
