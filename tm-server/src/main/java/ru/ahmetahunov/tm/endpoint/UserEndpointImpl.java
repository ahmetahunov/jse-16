package ru.ahmetahunov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.UserEndpoint;
import ru.ahmetahunov.tm.api.service.IPropertyService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.dto.SessionDTO;
import ru.ahmetahunov.tm.dto.UserDTO;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.util.CipherUtil;
import ru.ahmetahunov.tm.util.PassUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.UserEndpoint")
public final class UserEndpointImpl implements UserEndpoint {

	private ServiceLocator serviceLocator;

	public UserEndpointImpl(@NotNull ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	@Nullable
	@Override
	@WebMethod
	public UserDTO createUser(
			@WebParam(name = "login") final String login,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final User user = new User();
		if (login != null) user.setLogin(login);
		user.setPassword(PassUtil.getHash(password));
		serviceLocator.getUserService().persist(user);
		@Nullable final User found = serviceLocator.getUserService().findOne(user.getId());
		return (found == null) ? null : found.transformToDTO();
	}

	@Override
	@WebMethod
	public void updatePassword(
			@WebParam(name = "session") final String token,
			@WebParam(name = "old") final String oldPassword,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(sessionDTO);
		serviceLocator.getUserService().updatePassword(sessionDTO.getUserId(), oldPassword, password);
	}

	@Override
	@WebMethod
	public void updateLogin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "login") final String login
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(sessionDTO);
		serviceLocator.getUserService().updateLogin(sessionDTO.getUserId(), login);
	}

	@Override
	@WebMethod
	public UserDTO findUser(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(sessionDTO);
		@Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
		return (user == null) ? null : user.transformToDTO();
	}

}
