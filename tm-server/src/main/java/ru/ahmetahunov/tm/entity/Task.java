package ru.ahmetahunov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.dto.TaskDTO;
import ru.ahmetahunov.tm.enumerated.Status;
import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task extends AbstractEntity {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    @Column(name = "start_date")
    private Date startDate = new Date(0);

    @NotNull
    @Column(name = "finish_date")
    private Date finishDate = new Date(0);

    @NotNull
    @Column(name = "creation_date")
    private Date creationDate = new Date(System.currentTimeMillis());

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    public TaskDTO transformToDTO() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setId(this.id);
        task.setName(this.name);
        task.setDescription(this.description);
        task.setStartDate(this.startDate);
        task.setFinishDate(this.finishDate);
        task.setCreationDate(this.creationDate);
        task.setStatus(this.status);
        task.setProjectId(this.project.getId());
        task.setUserId(this.user.getId());
        return task;
    }

}
