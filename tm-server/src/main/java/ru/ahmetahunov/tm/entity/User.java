package ru.ahmetahunov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.dto.UserDTO;
import ru.ahmetahunov.tm.enumerated.Role;
import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class User extends AbstractEntity {

    @NotNull
    @Column(unique = true)
    private String login = "";

    @NotNull
    private String password = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<Project> projects;

    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<Task> tasks;

    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<Session> session;

    @NotNull
    public UserDTO transformToDTO() {
        @NotNull final UserDTO user = new UserDTO();
        user.setId(this.id);
        user.setLogin(this.login);
        user.setPassword(this.password);
        user.setRole(this.role);
        return user;
    }

}
