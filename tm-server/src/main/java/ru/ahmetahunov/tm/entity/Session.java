package ru.ahmetahunov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.dto.SessionDTO;
import javax.persistence.*;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Session extends AbstractEntity {

	@NotNull
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@Nullable
	private String signature;

	private long timestamp = System.currentTimeMillis();

	@NotNull
	public SessionDTO transformToDTO() {
		@NotNull final SessionDTO session = new SessionDTO();
		session.setId(this.id);
		session.setUserId(this.user.getId());
		session.setRole(this.user.getRole());
		session.setSignature(this.signature);
		session.setTimestamp(this.timestamp);
		return session;
	}

}
