package ru.ahmetahunov.tm.repository;

import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ISessionRepository;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@RequiredArgsConstructor
public class SessionRepository implements ISessionRepository {

	@NotNull
	private final EntityManager entityManager;

	@Override
	public void persist(@NotNull final Session session) {
		entityManager.persist(session);
	}

	@Override
	public void merge(@NotNull final Session session) {
		entityManager.merge(session);
	}

	@Nullable
	@Override
	public Session findOne(@NotNull final String id) {
		@NotNull final TypedQuery<Session> query = entityManager.createQuery(
				"SELECT s FROM Session s WHERE s.id = :id",
				Session.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("id", id);
		@NotNull final List<Session> sessions = query.getResultList();
		return (sessions.isEmpty()) ? null : sessions.get(0);
	}

	@Nullable
	@Override
	public Session findOneByUserId(@NotNull final String id, @NotNull final String userId) {
		@NotNull final TypedQuery<Session> query = entityManager.createQuery(
				"SELECT s FROM Session s WHERE s.user.id = :userId AND s.id = :id",
				Session.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("userId", userId);
		query.setParameter("id", id);
		@NotNull final List<Session> sessions = query.getResultList();
		return (sessions.isEmpty()) ? null : sessions.get(0);
	}

	@NotNull
	@Override
	public List<Session> findAll() {
		@NotNull final TypedQuery<Session> query = entityManager.createQuery(
				"SELECT s FROM Session s",
				Session.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		return query.getResultList();
	}

	@NotNull
	@Override
	public List<Session> findAll(@NotNull final String userId) {
		@NotNull final TypedQuery<Session> query = entityManager.createQuery(
				"SELECT s FROM Session s WHERE s.user.id = :userId",
				Session.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("userId", userId);
		return query.getResultList();
	}

	@Override
	public void remove(@NotNull final String id) throws InterruptedOperationException {
		@Nullable final Session session = findOne(id);
		if (session == null) throw new InterruptedOperationException();
		entityManager.remove(session);
	}

	@Override
	public void removeByUserId(@NotNull final String id, @NotNull final String userId) throws InterruptedOperationException {
		@Nullable final Session session = findOneByUserId(id, userId);
		if (session == null) throw new InterruptedOperationException();
		entityManager.remove(session);
	}

}
