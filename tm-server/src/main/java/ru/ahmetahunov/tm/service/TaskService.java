package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.EntityManagerService;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.repository.TaskRepository;
import ru.ahmetahunov.tm.util.ComparatorUtil;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(@NotNull final EntityManagerService entityManagerService) {
        super(entityManagerService);
    }

    @Override
    public void persist(@Nullable final Task task) throws InterruptedOperationException {
        if (task == null) throw new InterruptedOperationException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.persist(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@Nullable final Task task) throws InterruptedOperationException {
        if (task == null) throw new InterruptedOperationException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.merge(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @Nullable final Task task = repository.findOne(id);
        entityManager.close();
        return task;
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @Nullable final Task task = repository.findOneById(userId, taskId);
        entityManager.close();
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> tasks = repository.findAll();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> tasks = repository.findAllByUserId(userId);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable String comparator) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> tasks =
                repository.findAllWithComparator(userId, ComparatorUtil.getComparator(comparator));
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable String comparator
    ) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> tasks =
                repository.findAllByProjectId(userId, projectId, ComparatorUtil.getComparator(comparator));
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findByName(@Nullable final String userId, @Nullable final String taskName) {
        if (taskName == null || taskName.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> tasks = repository.findByName(userId, "%" + taskName + "%");
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findByDescription(@Nullable final String userId, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (description == null || description.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> tasks = repository.findByDescription(userId, "%" + description + "%");
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDesc(@Nullable final String userId, @Nullable final String searchPhrase) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (searchPhrase == null || searchPhrase.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> tasks = repository.findByNameOrDesc(userId, "%" + searchPhrase + "%");
        entityManager.close();
        return tasks;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) throws InterruptedOperationException {
        if (taskId == null || taskId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.removeById(userId, taskId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(@Nullable final String id) throws InterruptedOperationException {
        if (id == null || id.isEmpty()) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.removeAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
