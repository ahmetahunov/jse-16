package ru.ahmetahunov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.enumerated.Status;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public final class ProjectDTO extends AbstractEntityDTO implements Serializable {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date startDate = new Date(0);

    @NotNull
    private Date finishDate = new Date(0);

    @NotNull
    private Date creationDate = new Date(System.currentTimeMillis());

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private String userId = "";

    @NotNull
    public Project transformToEntity(@NotNull final ServiceLocator serviceLocator) {
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final Project project = new Project();
        project.setId(this.id);
        project.setName(this.name);
        project.setDescription(this.description);
        project.setStartDate(this.startDate);
        project.setFinishDate(this.finishDate);
        project.setCreationDate(this.creationDate);
        project.setStatus(this.status);
        project.setTasks(taskService.findAll(this.userId, this.id, "name"));
        project.setUser(userService.findOne(this.userId));
        return project;
    }

}
